from peeweecfg import db, Entity

from time import time
from peewee import (
	Model, CompositeKey, ForeignKeyField,
	BigAutoField, BigIntegerField, CharField, TextField
)


class Board(Entity):
	code = CharField(primary_key=True)
	name = CharField()


class Thread(Entity):
	num = BigAutoField()
	subj = CharField()
	board = ForeignKeyField(Board, backref='threads')


class Post(Entity):
	num = BigAutoField()
	text = TextField()
	time = BigIntegerField(default=time)
	thread = ForeignKeyField(Thread, backref='posts')


class Reply(Entity):
	cite = ForeignKeyField(Post, backref='replies')
	reply = ForeignKeyField(Post, backref='cites')

	class Meta:
		primary_key = CompositeKey('cite', 'reply')


def init():
	db.connect()
	db.create_tables([Board, Thread, Post, Reply])
	db.close()
