from peewee import SqliteDatabase, Model

db = SqliteDatabase('../hsech.db')


class Entity(Model):
	class Meta:
		database = db


class PeeweeMiddleware:
	def process_resource(self, req, resp, resource, params):
		db.connect()

	def process_response(self, req, resp, resource, req_succeeded):
		if not db.is_closed():
			db.close()
