from models import Board, Thread, Post, Reply

from falcon import HTTPNotFound, HTTPBadRequest
from peewee import DoesNotExist
from playhouse.shortcuts import model_to_dict


class BoardResource:
	def on_get_collection(self, req, resp):
		boards = Board.select()

		resp.media = [model_to_dict(board) for board in boards]

	def on_get(self, req, resp):
		code = req.get_param('code', required=True)
		try:
			board = Board.get_by_id(code)
		except DoesNotExist:
			raise HTTPNotFound

		resp.media = model_to_dict(board)


class ThreadResource:
	def on_get_collection(self, req, resp):
		code = req.get_param('code')
		if code:
			try:
				threads = Board.get_by_id(code).threads
			except DoesNotExist:
				raise HTTPNotFound
		else:
			threads = Thread.select()

		for thread in threads:
			thread.posts = thread.posts[:3]

		resp.media = [model_to_dict(thread, backrefs=True) for thread in threads]

	def on_get(self, req, resp):
		num = req.get_param_as_int('num', required=True)
		try:
			thread = Thread.get_by_id(num)
		except DoesNotExist:
			raise HTTPNotFound

		resp.media = model_to_dict(thread, backrefs=True)

	def on_post(self, req, resp):
		code = req.get_param('code', required=True)
		try:
			board = Board.get_by_id(code)
		except DoesNotExist:
			raise HTTPNotFound

		subj = req.get_param('subj', required=True)
		text = req.get_param('text', required=True)
		if not (subj and text):
			raise HTTPBadRequest

		thread = Thread.create(subj=subj, board=board)
		post = Post.create(text=text, thread=thread)

		resp.media = model_to_dict(thread)


class PostResource:
	def on_get_search(self, req, resp):
		text = req.get_param('text', required=True)
		posts = Post.select().where(Post.text.contains(text))

		resp.media = [model_to_dict(posts) for posts in posts]

	def on_get_collection(self, req, resp):
		num = req.get_param_as_int('num', required=True)
		try:
			posts = Thread.get_by_id(num).posts
		except DoesNotExist:
			raise HTTPNotFound

		resp.media = [model_to_dict(post) for post in posts]

	def on_get(self, req, resp):
		num = req.get_param_as_int('num', required=True)
		try:
			post = Post.get_by_id(num)
		except DoesNotExist:
			raise HTTPNotFound

		resp.media = model_to_dict(post)

	def on_post(self, req, resp):
		num = req.get_param_as_int('num', required=True)
		try:
			thread = Thread.get_by_id(num)
		except DoesNotExist:
			raise HTTPNotFound

		text = req.get_param('text', required=True)
		if not text:
			raise HTTPBadRequest

		post = Post.create(text=text, thread=thread)

		resp.media = model_to_dict(post)
