from peeweecfg import PeeweeMiddleware
from resources import BoardResource, ThreadResource, PostResource
import models

from falcon import API


def init():
	models.init()
	api = API(middleware=[PeeweeMiddleware()])
	api.req_options.auto_parse_form_urlencoded = True
	api.add_route('/boards', BoardResource(), suffix='collection')
	api.add_route('/board', BoardResource())
	api.add_route('/threads', ThreadResource(), suffix='collection')
	api.add_route('/thread', ThreadResource())
	api.add_route('/search', PostResource(), suffix='search')
	api.add_route('/posts', PostResource(), suffix='collection')
	api.add_route('/post', PostResource())
	return api


api = init()
